# These libs are likely part of your distro
# So you only need to include this module-set if you compile your own Qt6,
# to avoid mixing your own Qt with the distro's Qt.

module-set custom-qt6-libs
    repository _  # just to allow the module-set to be validated
    use-modules libgpg-error gpgme poppler libaccounts-qt signond packagekit-qt libsignon-glib qtkeychain libquotient cmark \
        kdsoap qcoro appstream wayland wayland-protocols qmltermwidget kcolorpicker kimageannotator
end module-set

# ---

# keep libgpg-error in sync with gpgme, see below
options libgpg-error
    repository https://dev.gnupg.org/source/libgpg-error.git
    branch master
    # NOTE: you need to run ./autogen.sh before kdesrc-build runs configure.
    # Maybe we can ask kdesrc-build do to that?
    configure-flags --enable-maintainer-mode
end options

# qgpgme is part of gpgme...
options gpgme
    repository https://dev.gnupg.org/source/gpgme.git
    branch master
    # NOTE: you need to run ./autogen.sh before kdesrc-build runs configure.
    # Maybe we can ask kdesrc-build do to that?
    configure-flags --enable-maintainer-mode --enable-languages=cpp,qt6
end options

# for KFileMetaData
options poppler
  repository https://gitlab.freedesktop.org/poppler/poppler.git
  branch master
  cmake-options -DWITH_GLIB=OFF -DENABLE_UNSTABLE_API_ABI_HEADERS=1 -DENABLE_QT5=OFF -DENABLE_QT6=ON
end options

# For kaccounts-integration

options libaccounts-qt
  repository https://gitlab.com/accounts-sso/libaccounts-qt.git
  branch master
  qmake-options PREFIX=${install-dir}
  override-build-system qmake6
end options

# upstream isn't ported to Qt6 yet, use fork meanwhile
options signond
  repository https://gitlab.com/nicolasfella/signond.git
  branch qt6
  qmake-options PREFIX=${install-dir} CONFIG+=no_etc
  override-build-system qmake6
end options

# For apper

options packagekit-qt
  repository https://github.com/PackageKit/PackageKit-Qt.git
  cmake-options -DBUILD_WITH_QT6=ON
  branch main
end options

options libsignon-glib
  # this one does depend on qt, it seems
  repository https://gitlab.com/accounts-sso/libsignon-glib.git
  qmake-options PREFIX=${install-dir}
end options

# Mandatory for krita

#options quazip
#  repository https://github.com/stachenov/quazip.git
#  branch master
#end options

# For kaidan

#options qxmpp
#  repository https://github.com/qxmpp-project/qxmpp.git
#  branch master
#  cmake-options -DBUILD_TESTS=OFF -DBUILD_EXAMPLES=OFF
#end options

# For neochat

options qtkeychain
  cmake-options -DBUILD_WITH_QT6=ON
  repository https://github.com/frankosterfeld/qtkeychain.git
  branch main
end options

options libquotient
  repository https://github.com/quotient-im/libQuotient
  branch 0.8.2
  cmake-options -DBUILD_SHARED_LIBS=ON -DBUILD_WITH_QT6=ON -DQuotient_ENABLE_E2EE=on
end options

options cmark
  repository https://github.com/commonmark/cmark.git
  branch master
end options

# for kio-extras
options kdsoap
  repository https://github.com/KDAB/KDSoap
  cmake-options -DKDSoap_QT6=true
  branch master
end options

# For neochat, spacebar, possibly more
options qcoro
  repository https://github.com/danvratil/qcoro
  cmake-options -DUSE_QT_VERSION=6 -DBUILD_SHARED_LIBS=ON
  branch main
end options

options appstream
  repository https://github.com/ximion/appstream
  configure-flags -Dqt=true
  set-env LDFLAGS -Wl,-rpath=${install-dir}/${libname}
  branch main
end options

options wayland
  repository https://gitlab.freedesktop.org/wayland/wayland
  branch main
end options

options wayland-protocols
  repository https://gitlab.freedesktop.org/wayland/wayland-protocols
  branch main
end options

# For qmlkonsole
options qmltermwidget
  repository https://invent.kde.org/jbbgameich/qmltermwidget
  branch master
  cmake-options -DBUILD_SHARED_LIBS=ON -DBUILD_WITH_QT6=ON
end options

options kcolorpicker
  repository https://github.com/ksnip/kColorPicker
  branch master
  cmake-options -DBUILD_SHARED_LIBS=ON -DBUILD_WITH_QT6=ON
end options

options kimageannotator
  repository https://github.com/ksnip/kImageAnnotator
  branch master
  cmake-options -DBUILD_SHARED_LIBS=ON -DBUILD_WITH_QT6=ON
end options

# kate: syntax kdesrc-buildrc;
